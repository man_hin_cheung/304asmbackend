const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;

exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send({status:"success"});
};

// Update a Profile by the id in the request
exports.updateProfile = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  var user = {
    profilecontent:req.body.profilecontent
};
User.findByIdAndUpdate(req.params.id, { $set: user }, { new: true }, (err, doc) => {
    if (!err) { 
      res.send({ message: "Content was updated successfully. The content Will update at next time you login" });
     } else { 
      console.log('Error in Update :' + JSON.stringify(err, undefined, 2));
     }
});
}
/////////////////


exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found User with id " + id });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving User with id=" + id });
    });
};

