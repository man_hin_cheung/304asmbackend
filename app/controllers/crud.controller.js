const db = require("../models");
const Crud = db.cruds;
// Create and Save a new Topic
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    // Create a Topic
    const crud = new Crud({
      title: req.body.title,
      description: req.body.description,
      username: req.body.username,
      published: req.body.published ? req.body.published : false
    });
  
    // Save Topic in the database
    crud
      .save(crud)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Topic."
        });
      });
  };
  
  // Retrieve all Topics from the database.
  exports.findAll = (req, res) => {
    const title = req.query.title;
    var condition = title ? { title: { $regex: new RegExp(title), $options: "i" } } : {};
  
    Crud.find(condition)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving topics."
        });
      });
  };
  
  // Find a single Topic with an id
  exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Crud.findById(id)
      .then(data => {
        if (!data)
          res.status(404).send({ message: "Not found Topic with id " + id });
        else res.send(data);
      })
      .catch(err => {
        res
          .status(500)
          .send({ message: "Error retrieving Topic with id=" + id });
      });
  };

  
  // Update a Topic by the id in the request
  exports.update = (req, res) => {
    if (!req.body) {
      return res.status(400).send({
        message: "Data to update can not be empty!"
      });
    }
  
    const id = req.params.id;
  
    Crud.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: `Cannot update Topic with id=${id}. Maybe Topic was not found!`
          });
        } else res.send({ message: "Topic was updated successfully." });
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Topic with id=" + id
        });
      });
  };
  
  // Delete a Topic with the specified id in the request
  exports.delete = (req, res) => {
    const id = req.params.id;
  
    Crud.findByIdAndRemove(id, { useFindAndModify: false })
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: `Cannot delete Topic with id=${id}. Maybe Topic was not found!`
          });
        } else {
          res.send({
            message: "Topic was deleted successfully!"
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Topic with id=" + id
        });
      });
  };
  
  // Delete all Topics from the database.
  exports.deleteAll = (req, res) => {
    Crud.deleteMany({})
      .then(data => {
        res.send({
          message: `${data.deletedCount} Topic were deleted successfully!`
        });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all topics."
        });
      });
  };
  
  // Find all published Topics
  exports.findAllPublished = (req, res) => {
    Crud.find({ published: true })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving topic."
        });
      });
  };