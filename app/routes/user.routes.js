const { authJwt } = require("../middlewares");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/all", controller.allAccess);

  //search user by id 
  app.get("/api/test/user/profile/:id", controller.findOne);


  app.get("/api/test/user", [authJwt.verifyToken], controller.userBoard);

  // Update a updateprofile with id
  app.put("/api/test/user/profile/:id", controller.updateProfile);

  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );
};
