module.exports = app => {
    const cruds = require("../controllers/crud.controller.js");
  
    var router = require("express").Router();
  
    // Create a new Topic
    router.post("/", cruds.create);
  
    // Retrieve all topics
    router.get("/", cruds.findAll);
  
    // Retrieve all published topics
    router.get("/published", cruds.findAllPublished);
  
    // Retrieve a single topic with id
    router.get("/:id", cruds.findOne);
  
    // Update a topic with id
    router.put("/:id", cruds.update);
  
    // Delete a topic with id
    router.delete("/:id", cruds.delete);
  
    // Delete all topic
    router.delete("/", cruds.deleteAll);
  
    app.use("/api/cruds", router);
  };
  